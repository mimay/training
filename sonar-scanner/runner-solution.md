# Solution for the sonar scanner 
```yaml
stages:
- sonar-scanner
sonar-scanner:
  stage: sonar-scanner
  image: ciricihq/gitlab-sonar-scanner
  variables:
    SONAR_URL: <url>
    SONAR_ANALYSIS_MODE: publish
    SONAR_TOKEN: <token>
    SONAR_PROJECT_KEY: <key>
    SONAR_SOURCES: <sources>
  script: 
  - gitlab-sonar-scanner
```
